################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/adc_init.c \
../src/clock_init.c \
../src/dma_init.c \
../src/gpio_init.c \
../src/interrupt_init.c \
../src/main.c \
../src/stepper_calculaton.c \
../src/syscalls.c \
../src/system_stm32f10x.c \
../src/timer_init.c \
../src/uart_init.c 

OBJS += \
./src/adc_init.o \
./src/clock_init.o \
./src/dma_init.o \
./src/gpio_init.o \
./src/interrupt_init.o \
./src/main.o \
./src/stepper_calculaton.o \
./src/syscalls.o \
./src/system_stm32f10x.o \
./src/timer_init.o \
./src/uart_init.o 

C_DEPS += \
./src/adc_init.d \
./src/clock_init.d \
./src/dma_init.d \
./src/gpio_init.d \
./src/interrupt_init.d \
./src/main.d \
./src/stepper_calculaton.d \
./src/syscalls.d \
./src/system_stm32f10x.d \
./src/timer_init.d \
./src/uart_init.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DSTM32 -DSTM32F1 -DSTM32F103C8Tx -DDEBUG -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -I"C:/Users/Balazs/Documents/git/printer_st/printer_st/StdPeriph_Driver/inc" -I"C:/Users/Balazs/Documents/git/printer_st/printer_st/inc" -I"C:/Users/Balazs/Documents/git/printer_st/printer_st/CMSIS/device" -I"C:/Users/Balazs/Documents/git/printer_st/printer_st/CMSIS/core" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


