/*
 * timer_init.c
 *
 *  Created on: 2018. dec. 12.
 *      Author: Balazs
 */
#include "stm32f10x_tim.h"
#include "timer_init.h"
#include "adc_init.h"
#include "stepper_calculation.h"
#include "gpio_init.h"
#include "uart_init.h"

unsigned long desired_temp = 382;

unsigned long long buff_truncx = 1;
unsigned long long buff_truncy = 1;
unsigned long long buff_truncz = 1;
unsigned long long buff_trunce = 1;
unsigned long long buff_time_quantum_cycle = 0;

unsigned long long stepped_steps_x = 0;
unsigned long long stepped_steps_y = 0;
unsigned long long stepped_steps_z = 0;
unsigned long long stepped_steps_e = 0;

unsigned char stepping_in_progress = 0;

unsigned char index_to_read = 0;
unsigned char index_to_step = 0;


void init_timer1(void){
	TIM_TimeBaseInitTypeDef timer;

	timer.TIM_Prescaler=71; //n+1 division 1Mhz
	timer.TIM_ClockDivision=TIM_CKD_DIV1;
	timer.TIM_RepetitionCounter=0;
	timer.TIM_Period=8;   //~48kHz * 2

	timer.TIM_CounterMode=TIM_CounterMode_Up;

	TIM_DeInit(TIM1);
	TIM_TimeBaseInit(TIM1,&timer);
	TIM_ClearITPendingBit(TIM1,TIM_IT_Update);
	TIM_ITConfig(TIM1,TIM_IT_Update,ENABLE);
//	TIM_Cmd(TIM1,ENABLE);
}


void init_timer2(void){ //pwm for thermal control
	TIM_TimeBaseInitTypeDef timer_2;
	TIM_OCInitTypeDef timer_2_pwm;

	timer_2.TIM_Prescaler=71; //n+1 division 1MHz real
	timer_2.TIM_ClockDivision=TIM_CKD_DIV1;	//72MHz input
	timer_2.TIM_RepetitionCounter=0;		//update event generated, when this register is 0 (like a sub divider related to the interrupt event)
	timer_2.TIM_Period=999; 					// auto reload register timer overrun value (counting from 0!), 1kHz PWM
	timer_2.TIM_CounterMode=TIM_CounterMode_Up;

	timer_2_pwm.TIM_OCMode = TIM_OCMode_PWM1;
	timer_2_pwm.TIM_OutputState = TIM_OutputState_Enable;
	timer_2_pwm.TIM_Pulse = 500; 				//capture compare register - pwm duty cycle, config with "TIM_SetCompare1()" func.
	timer_2_pwm.TIM_OCPolarity = TIM_OCPolarity_High;

	TIM_DeInit(TIM2);
	TIM_TimeBaseInit(TIM2,&timer_2);
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);

	TIM_OC1Init(TIM2,&timer_2_pwm); 			//ch-1
//	TIM_ARRPreloadConfig(TIM2,ENABLE);

	TIM_Cmd(TIM2,ENABLE); ///////////////////

}

void init_timer3(void){ //control loop's interupt generator
	TIM_TimeBaseInitTypeDef timer_3;
//	TIM_OCInitTypeDef timer_3_pwm;

	timer_3.TIM_Prescaler=7199; //n+1 division 10kHz real
	timer_3.TIM_ClockDivision=TIM_CKD_DIV1;	//72MHz input
	timer_3.TIM_RepetitionCounter=0;		//update event generated, when this register is 0 (like a sub divider related to the interrupt event)
	timer_3.TIM_Period=1999; 					// auto reload register timer overrun value (counting from 0!), 5Hz PID cycle
	timer_3.TIM_CounterMode=TIM_CounterMode_Up;

//	timer_3_pwm.TIM_OCMode = TIM_OCMode_PWM1;
//	timer_3_pwm.TIM_OutputState = TIM_OutputState_Enable;
//	timer_3_pwm.TIM_Pulse = 1930; 				//capture compare register - pwm duty cycle, config with "TIM_SetCompare1()" func.
//	timer_3_pwm.TIM_OCPolarity = TIM_OCPolarity_High;

	TIM_DeInit(TIM3);
	TIM_TimeBaseInit(TIM3,&timer_3);
//	TIM_OC1Init(TIM3,&timer_3_pwm); 			//ch-1
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);

	TIM_Cmd(TIM3,ENABLE); ///////////////////

}



void TIM2_IRQHandler(void){
	TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
}

void TIM3_IRQHandler(void){
	signed short error;
	signed short command_pwm;
	static signed long long integral_error = 0;
	unsigned long actual_temp;

	ADC_ClearFlag(ADC1,ADC_FLAG_EOC);
	while(RESET==ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC)){}
	actual_temp = ADC1->DR;					//measurement #1

	ADC_ClearFlag(ADC1,ADC_FLAG_EOC);
	while(RESET==ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC)){}
	actual_temp = actual_temp + ADC1->DR;	//measurement #2
	actual_temp = actual_temp >> 1; 		//Division by 2 (average the 2 measurement)

	error = actual_temp - desired_temp;
//	integral_error = integral_error + error;
//	command_pwm = (error * P_CONST) + ((integral_error) * I_CONST);

	command_pwm = (error * P_CONST);

	if (command_pwm <=0){			//set limits
		command_pwm = 0;
	}

//	command_pwm = command_pwm >> 4;

	if (command_pwm >= 999){
			command_pwm = 999;
		}

	TIM_SetCompare1(TIM2,command_pwm); //set pwm
//	TIM_SetCompare1(TIM2,500);


	TIM_ClearITPendingBit(TIM3,TIM_IT_Update);
}

void TIM1_UP_IRQHandler(void){
	stepping_in_progress = 1; //stepping is in progress
	buff_truncx = buff_truncx + 1;
	buff_truncy = buff_truncy + 1;
	buff_truncz = buff_truncz + 1;
	buff_trunce = buff_trunce + 1;
	buff_time_quantum_cycle = buff_time_quantum_cycle + 1;
//--------------x-----------------
	if (buff_truncx == truncx){
		if(stepped_steps_x < stepsx){
			stepped_steps_x = stepped_steps_x + 1;
			GPIO_WriteBit(PORT_STP_X,PIN_STP_X,Bit_SET);
		}

	}
	else{
		GPIO_WriteBit(PORT_STP_X,PIN_STP_X,Bit_RESET);
	}

	if (buff_truncx == (truncx + 1)){
		buff_truncx = 1;
	}
//--------------y-----------------
	if (buff_truncy == truncy){
		if(stepped_steps_y < stepsy){
				stepped_steps_y = stepped_steps_y + 1;
				GPIO_WriteBit(PORT_STP_Y,PIN_STP_Y,Bit_SET);
			}
	}
	else{
		GPIO_WriteBit(PORT_STP_Y,PIN_STP_Y,Bit_RESET);
	}

	if (buff_truncy == (truncy + 1)){
		buff_truncy = 1;
	}
//--------------z-----------------
	if (buff_truncz == truncz){
		if(stepped_steps_z < stepsz){
				stepped_steps_z = stepped_steps_z + 1;
				GPIO_WriteBit(PORT_STP_Z,PIN_STP_Z,Bit_SET);
			}
	}
	else{
		GPIO_WriteBit(PORT_STP_Z,PIN_STP_Z,Bit_RESET);
	}

	if (buff_truncz == (truncz + 1)){
		buff_truncz = 1;
	}
//--------------e-----------------
	if (buff_trunce == trunce){
		if(stepped_steps_e < stepse){
				stepped_steps_e = stepped_steps_e + 1;
				GPIO_WriteBit(PORT_STP_E,PIN_STP_E,Bit_SET);
			}
	}
	else{
		GPIO_WriteBit(PORT_STP_E,PIN_STP_E,Bit_RESET);
	}

	if (buff_trunce == (trunce + 1)){
		buff_trunce = 1;
	}


	if (buff_time_quantum_cycle >= time_quantum_cycle){
		TIM_Cmd(TIM1,DISABLE);
		stepping_in_progress = 0; // end of stepping
		data[index_to_step].status = 0;
		index_to_step = index_to_step + 1;
		if (index_to_step > 9){
			index_to_step = 0;
			}
		}
	TIM_ClearITPendingBit(TIM1,TIM_IT_Update);
}
