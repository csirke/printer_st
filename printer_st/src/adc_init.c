/*
 * adc_init.c
 *
 *  Created on: 2019. jan. 14.
 *      Author: Balazs
 */
#include "stm32f10x_adc.h"


void adc_init_func(void){
	ADC_InitTypeDef adc_struct;

	ADC_DeInit(ADC1);
	adc_struct.ADC_Mode=ADC_Mode_Independent;
	adc_struct.ADC_ScanConvMode = DISABLE;
	adc_struct.ADC_ContinuousConvMode = ENABLE;
	adc_struct.ADC_DataAlign = ADC_DataAlign_Right;
	adc_struct.ADC_NbrOfChannel = 1;

	ADC_Init(ADC1,&adc_struct);
	ADC_RegularChannelConfig(ADC1,ADC_Channel_8,1,ADC_SampleTime_1Cycles5);
	ADC_Cmd(ADC1,ENABLE);
	ADC_StartCalibration(ADC1);
	while(SET==ADC_GetCalibrationStatus(ADC1));
	ADC_Cmd(ADC1,ENABLE); //start conversion


}


