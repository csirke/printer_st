/*
 * interrupt_init.c
 *
 *  Created on: 2018. dec. 12.
 *      Author: Balazs
 */
#include "misc.h"

void interrupt_init_func(void){

	NVIC_InitTypeDef nvicStructure;

	nvicStructure.NVIC_IRQChannel = TIM1_UP_IRQn;
	nvicStructure.NVIC_IRQChannelPreemptionPriority = 5;
	nvicStructure.NVIC_IRQChannelSubPriority = 5;
	nvicStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvicStructure);

	nvicStructure.NVIC_IRQChannel = TIM2_IRQn;
	nvicStructure.NVIC_IRQChannelPreemptionPriority = 5;
	nvicStructure.NVIC_IRQChannelSubPriority = 5;
	nvicStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvicStructure);

	nvicStructure.NVIC_IRQChannel = TIM3_IRQn;
	nvicStructure.NVIC_IRQChannelPreemptionPriority = 5;
	nvicStructure.NVIC_IRQChannelSubPriority = 5;
	nvicStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvicStructure);

	nvicStructure.NVIC_IRQChannel = USART1_IRQn;
	nvicStructure.NVIC_IRQChannelPreemptionPriority = 5;
	nvicStructure.NVIC_IRQChannelSubPriority = 5;
	nvicStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvicStructure);

	nvicStructure.NVIC_IRQChannel = DMA1_Channel5_IRQn;
	nvicStructure.NVIC_IRQChannelPreemptionPriority = 5;
	nvicStructure.NVIC_IRQChannelSubPriority = 5;
	nvicStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvicStructure);

}
