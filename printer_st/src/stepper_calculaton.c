/*
 * stepper_calculaton.c
 *
 *  Created on: 2019. febr. 5.
 *      Author: Balazs
 */
#include "dma_init.h"
#include "timer_init.h"
#include "gpio_init.h"
#include "stm32f10x_gpio.h"
#include "stepper_calculation.h"
#define TIME_QUANTUM_MULTIPLICATOR 8

unsigned long long time_quantum_cycle=0;

unsigned long long truncx = 0;
unsigned long long truncy = 0;
unsigned long long truncz = 0;
unsigned long long trunce = 0;

unsigned long long stepsx = 0;
unsigned long long stepsy = 0;
unsigned long long stepsz = 0;
unsigned long long stepse = 0;
//unsigned long long max=0;

struct decoded data[10];

struct decoded calc(struct message* dataframe){

	struct decoded return_buffer;

	unsigned long long stepsx_local = 0;
	unsigned long long stepsy_local = 0;
	unsigned long long stepsz_local = 0;
	unsigned long long stepse_local = 0;
	unsigned long long max=0;
	unsigned long long time_quantum_cycle_local=0;

	unsigned long long truncx_local = 0;
	unsigned long long truncy_local = 0;
	unsigned long long truncz_local = 0;
	unsigned long long trunce_local = 0;

	//make sure, that everthing is a positive number
	if ((dataframe->steps_x) < 0){
		stepsx_local = dataframe->steps_x * -1;
		return_buffer.DIR_X = 0;
	}
	else {
		stepsx_local = dataframe->steps_x;
		return_buffer.DIR_X = 1;
	}

	if ((dataframe->steps_y) < 0){
		stepsy_local = dataframe->steps_y * -1;
		return_buffer.DIR_Y = 0;
		}
	else {
		stepsy_local = dataframe->steps_y;
		return_buffer.DIR_Y = 1;
	}

	if ((dataframe->steps_z) < 0){
		stepsz_local = dataframe->steps_z * -1;
		return_buffer.DIR_Z = 0;
		}
	else {
		stepsz_local = dataframe->steps_z;
		return_buffer.DIR_Z = 1;
	}

	if ((dataframe->steps_e) < 0){
		stepse_local = dataframe->steps_e * -1;
		return_buffer.DIR_E = 0;
		}
	else {
		stepse_local = dataframe->steps_e;
		return_buffer.DIR_E = 1;
	}
	//select the maximum
	max = stepsx_local;

	if (max < stepsy_local){
		max = stepsy_local;
	}

	if (max < stepsz_local){
		max = stepsz_local;
	}

	if (max < stepse_local){
		max = stepse_local;
	}

	time_quantum_cycle_local = max * TIME_QUANTUM_MULTIPLICATOR;
	max = max * TIME_QUANTUM_MULTIPLICATOR;
	//trunc division, the min trunced number is equal to the multiplicator
	if (stepsx_local > 0){
		truncx_local = max / stepsx_local;
	}
		else {
			truncx_local = 0;
		}

	if (stepsy_local > 0){
		truncy_local = max / stepsy_local;
	}
		else {
			truncy_local = 0;
		}

	if (stepsz_local > 0){
			truncz_local = max / stepsz_local;
		}
		else {
			truncz_local = 0;
		}

	if (stepse_local > 0){
			trunce_local = max / stepse_local;
		}
		else {
			trunce_local = 0;
		}
	return_buffer.TIME_QUANTUM_CYCLE = time_quantum_cycle_local;

	return_buffer.STEPS_X = stepsx_local;
	return_buffer.STEPS_Y = stepsy_local;
	return_buffer.STEPS_Z = stepsz_local;
	return_buffer.STEPS_E = stepse_local;

	return_buffer.TRUNC_X = truncx_local;
	return_buffer.TRUNC_Y = truncy_local;
	return_buffer.TRUNC_Z = truncz_local;
	return_buffer.TRUNC_E = trunce_local;

	return_buffer.status = 1; // this is a fresh data

	return return_buffer;
}

void set_direction(struct decoded* dataframe){
	if ((dataframe->DIR_X) < 1){
		GPIO_WriteBit(PORT_DIR_X,PIN_DIR_X,Bit_RESET);
	}
	else{
		GPIO_WriteBit(PORT_DIR_X,PIN_DIR_X,Bit_SET);
	}

	if ((dataframe->DIR_Y) < 1){
		GPIO_WriteBit(PORT_DIR_Y,PIN_DIR_Y,Bit_RESET);
	}
	else{
		GPIO_WriteBit(PORT_DIR_Y,PIN_DIR_Y,Bit_SET);
	}

	if ((dataframe->DIR_Z) < 1){
		GPIO_WriteBit(PORT_DIR_Z,PIN_DIR_Z,Bit_RESET);
	}
	else{
		GPIO_WriteBit(PORT_DIR_Z,PIN_DIR_Z,Bit_SET);
	}

	if ((dataframe->DIR_E) < 1){
		GPIO_WriteBit(PORT_DIR_E,PIN_DIR_E,Bit_RESET);
	}
	else{
		GPIO_WriteBit(PORT_DIR_E,PIN_DIR_E,Bit_SET);
	}
}

void set_global_variables(struct decoded* dataframe){
	buff_truncx = 1;
	buff_truncy = 1;
	buff_truncz = 1;
	buff_trunce = 1;
	buff_time_quantum_cycle = 0;
	stepped_steps_x = 0;
	stepped_steps_y = 0;
	stepped_steps_z = 0;
	stepped_steps_e = 0;

	truncx = dataframe->TRUNC_X;
	truncy = dataframe->TRUNC_Y;
	truncz = dataframe->TRUNC_Z;
	trunce = dataframe->TRUNC_E;

	stepsx = dataframe->STEPS_X;
	stepsy = dataframe->STEPS_Y;
	stepsz = dataframe->STEPS_Z;
	stepse = dataframe->STEPS_E;

	time_quantum_cycle = dataframe->TIME_QUANTUM_CYCLE;

//	truncx = 0;
//	truncy = 0;
//	truncz = 8;
//	trunce = 0;
//
//	stepsx = 0;
//	stepsy = 0;
//	stepsz = 600000;
//	stepse = 0;
}
