/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/
#include "interrupt_init.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "clock_init.h"
#include "timer_init.h"
#include "gpio_init.h"
#include "stm32f10x_dma.h"
#include "uart_init.h"
#include "dma_init.h"
#include "stdio.h"
#include "adc_init.h"
#include "stepper_calculation.h"
			
volatile long szamlalo=0;



int main(void)
{
	clock_init_to72mhz();
	interrupt_init_func();
	init_timer1();
	gpio_init_func();
	uart1_dma_init();
	dma_uart1_init();
	adc_init_func();
	init_timer2();
	init_timer3();

	unsigned int adc_buffer;
	unsigned char free_message_sent = 0; //0: when not sent / 1: sent

	for (unsigned char i = 0; i < 10; i++){
		data[i].status = 0;
		}

	while(1){
//		for (unsigned int i =0; i<30000; i++){}
//		ADC_ClearFlag(ADC1,ADC_FLAG_EOC);
//		while(RESET==ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC)){}
////			actual_temp = ADC1->DR;
//			adc_buffer = ADC1->DR;
//			USART_SendData(USART1,adc_buffer);
//			ADC_ClearFlag(ADC1,ADC_FLAG_EOC);
//			while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
//			USART_SendData(USART1,adc_buffer>>8);

		if (free_message_sent == 0){
			if (data[index_to_read].status == 0){ //free buffer
				send_free_message();
				free_message_sent = 1; //sent flag set
			}
		}

		if ((free_message_sent == 1) && (message_received == 1)) {
			message_received = 0;
			free_message_sent = 0;
			data[index_to_read] = calc(&received_message.message_struct);
			index_to_read = index_to_read + 1;
			if (index_to_read > 9){
				index_to_read = 0;
			}
		}

		if ((stepping_in_progress == 0) && (data[index_to_step].status == 1)){
			set_direction(&data[index_to_step]);
			set_global_variables(&data[index_to_step]);
			TIM_Cmd(TIM1,ENABLE);
		}

//		if (message_received==1){
//			test = calc(&received_message.message_struct);
//			set_direction(&test);
//			set_global_variables(&test);
////			send_busy_message();
//			TIM_Cmd(TIM1,ENABLE);
//			message_received = 0;
//		}



	}
}
//
//void USART1_IRQHandler(void){
//	int a;
//	a = USART_ReceiveData(USART1);
////	USART_SendData(USART1,a+1);
//	USART_ClearITPendingBit(USART1,USART_IT_RXNE);
//}

