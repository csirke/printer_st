/*
 * gpio_init.c
 *
 *  Created on: 2018. dec. 12.
 *      Author: Balazs
 */
#include "stm32f10x_gpio.h"
#include "gpio_init.h"

void gpio_init_func(void){

	GPIO_InitTypeDef gpio;

	GPIO_DeInit(GPIOC);
	GPIO_DeInit(GPIOA);
	GPIO_DeInit(GPIOB);

	gpio.GPIO_Mode=  GPIO_Mode_Out_PP;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = GPIO_Pin_13;			//onboard LED
	GPIO_Init(GPIOC,&gpio);

	gpio.GPIO_Mode=  GPIO_Mode_AF_PP;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = GPIO_Pin_0;				//PWM - ch1 timer2
	GPIO_Init(GPIOA,&gpio);

	gpio.GPIO_Mode=  GPIO_Mode_AF_PP;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = GPIO_Pin_6;				//PWM - ch1 timer3
	GPIO_Init(GPIOA,&gpio);

	gpio.GPIO_Mode=  GPIO_Mode_IN_FLOATING;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;		//serial RX
	gpio.GPIO_Pin = GPIO_Pin_10;
	GPIO_Init(GPIOA,&gpio);

	gpio.GPIO_Mode=  GPIO_Mode_AF_PP;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = GPIO_Pin_9;				//serial TX
	GPIO_Init(GPIOA,&gpio);

	gpio.GPIO_Mode = GPIO_Mode_AIN;			//analog in
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOB,&gpio);

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;			//x stp
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = PIN_STP_X;
	GPIO_Init(PORT_STP_X,&gpio);

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;			//y stp
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = PIN_STP_Y;
	GPIO_Init(PORT_STP_Y,&gpio);

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;			//z stp
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = PIN_STP_Z;
	GPIO_Init(PORT_STP_Z,&gpio);

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;			//e stp
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = PIN_STP_E;
	GPIO_Init(PORT_STP_E,&gpio);

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;			//x dir
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = PIN_DIR_X;
	GPIO_Init(PORT_DIR_X,&gpio);

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;			//y dir
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = PIN_DIR_Y;
	GPIO_Init(PORT_DIR_Y,&gpio);

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;			//z dir
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = PIN_DIR_Z;
	GPIO_Init(PORT_DIR_Z,&gpio);

	gpio.GPIO_Mode = GPIO_Mode_Out_PP;			//e dir
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = PIN_DIR_E;
	GPIO_Init(PORT_DIR_E,&gpio);
}
