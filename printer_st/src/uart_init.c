/*
 * uart_init.c
 *
 *  Created on: 2018. dec. 15.
 *      Author: Balazs
 */
#include "stm32f10x_usart.h"

void uart1_dma_init(void){
	USART_InitTypeDef uart1;

	uart1.USART_BaudRate = 230400;
	uart1.USART_WordLength = USART_WordLength_8b;
	uart1.USART_StopBits = USART_StopBits_1;
	uart1.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	uart1.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	uart1.USART_Parity = USART_Parity_No;

	USART_DeInit(USART1);
	USART_Init(USART1,&uart1);
//	USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
	USART_DMACmd(USART1,USART_DMAReq_Rx,ENABLE); //enable DMA operation (rx on dma1 ch5)
	USART_Cmd(USART1,ENABLE);
}

void send_busy_message(void){
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
	USART_SendData(USART1,93);
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
	USART_SendData(USART1,129);
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
	USART_SendData(USART1,24);
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
	USART_SendData(USART1,109);
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
}

void send_free_message(void){
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
	USART_SendData(USART1,93);
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
	USART_SendData(USART1,104);
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
	USART_SendData(USART1,56);
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
	USART_SendData(USART1,109);
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
}

void send_resend_request_message(void){
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
	USART_SendData(USART1,93);
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
	USART_SendData(USART1,99);
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
	USART_SendData(USART1,61);
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
	USART_SendData(USART1,109);
	while(RESET==USART_GetFlagStatus(USART1,USART_FLAG_TXE)){}
}


