/*
 * dma_init.c
 *
 *  Created on: 2018. dec. 15.
 *      Author: Balazs
 */
#include "stm32f10x_dma.h"
#include "stm32f10x_gpio.h"
#include "dma_init.h"

unsigned char uart_dma_test;

union message_dataframe received_message;
union message_dataframe previously_received_message;
unsigned char message_received = 0;	//0 is false 1 is true

void dma_uart1_init(void){

	DMA_DeInit(DMA1_Channel5); //DMA1 ch5 is hard wired to usart1 (along with other peripherals)

	DMA_InitTypeDef dma_uarthoz;

	dma_uarthoz.DMA_MemoryBaseAddr = (long int)&received_message; //address of location
	dma_uarthoz.DMA_PeripheralBaseAddr = (long int)&USART1->DR; //address of origin (pheripheral data register)
	dma_uarthoz.DMA_Mode = DMA_Mode_Circular;			//autoreload the buffer size register after transfer complete
	dma_uarthoz.DMA_DIR = DMA_DIR_PeripheralSRC;		//pheripheral as source
	dma_uarthoz.DMA_PeripheralInc = DMA_PeripheralInc_Disable;	//do not increment the address of pheripheral
	dma_uarthoz.DMA_MemoryInc = DMA_MemoryInc_Enable;
	dma_uarthoz.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;	//the default unit is byte
	dma_uarthoz.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	dma_uarthoz.DMA_Priority = DMA_Priority_Medium;
	dma_uarthoz.DMA_M2M = DMA_M2M_Disable;					// not memory to memory transfer
	dma_uarthoz.DMA_BufferSize = 22;							//how many bytes do we want to transfer?

	DMA_Init(DMA1_Channel5,&dma_uarthoz);			//init dma channel
	DMA_ITConfig(DMA1_Channel5,DMA_IT_TC,ENABLE); //transfer complete interrupt
	DMA_Cmd(DMA1_Channel5,ENABLE);
}

void DMA1_Channel5_IRQHandler(void){				//interrupt handling
	GPIO_WriteBit(GPIOC,GPIO_Pin_13,Bit_SET);		//measure time requirement

	unsigned short int checksum_calculated = 0;
	unsigned char i = 0;

	checksum_calculated = received_message.words[0];

	for(i=1; i<10; i++){
		checksum_calculated = checksum_calculated ^ received_message.words[i];
	}

	if ((checksum_calculated == received_message.message_struct.checksum) || ((received_message.message_struct.checksum == 321) && (received_message.message_struct.message_id == 123))) {
		previously_received_message = received_message;
		message_received = 1;
	}
	else{
		//send_resend_request_message();
	}

	DMA_ClearITPendingBit(DMA1_IT_TC5);
	GPIO_WriteBit(GPIOC,GPIO_Pin_13,Bit_RESET);			//measure time requirement
//	USART_SendData(USART1,recieved_message.bytes[0]);
}
