/*
 * uart_init.h
 *
 *  Created on: 2018. dec. 15.
 *      Author: Balazs
 */

#ifndef UART_INIT_H_
#define UART_INIT_H_

void uart1_dma_init(void);
void send_busy_message(void);
void send_free_message(void);
void send_resend_request_message(void);


#endif /* UART_INIT_H_ */
