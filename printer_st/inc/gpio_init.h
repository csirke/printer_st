/*
 * gpio_init.h
 *
 *  Created on: 2018. dec. 12.
 *      Author: Balazs
 */

#ifndef GPIO_INIT_H_
#define GPIO_INIT_H_

#define PIN_STP_X GPIO_Pin_9
#define PIN_STP_Y GPIO_Pin_8
#define PIN_STP_Z GPIO_Pin_7
#define PIN_STP_E GPIO_Pin_6

#define PORT_STP_X GPIOB
#define PORT_STP_Y GPIOB
#define PORT_STP_Z GPIOB
#define PORT_STP_E GPIOB

#define PIN_DIR_X GPIO_Pin_5
#define PIN_DIR_Y GPIO_Pin_1
#define PIN_DIR_Z GPIO_Pin_2
#define PIN_DIR_E GPIO_Pin_3

#define PORT_DIR_X GPIOB
#define PORT_DIR_Y GPIOA
#define PORT_DIR_Z GPIOA
#define PORT_DIR_E GPIOA


void gpio_init_func(void);

#endif /* GPIO_INIT_H_ */
