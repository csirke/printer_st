/*
 * dma_init.h
 *
 *  Created on: 2018. dec. 15.
 *      Author: Balazs
 */

#ifndef DMA_INIT_H_
#define DMA_INIT_H_

extern unsigned char uart_dma_test;
extern unsigned char message_received;

struct message{
	int steps_x;
	int steps_y;
	int steps_z;
	int steps_e;
	unsigned short int steps_t;
	unsigned short int message_id;
	unsigned short int checksum;
};

union message_dataframe{
	unsigned short int words[11];
	unsigned char bytes[22];
	struct message message_struct;
};

extern union message_dataframe received_message;
extern union message_dataframe previously_received_message;



void dma_uart1_init(void);


#endif /* DMA_INIT_H_ */
