/*
 * stepper_calculation.h
 *
 *  Created on: 2019. febr. 5.
 *      Author: Balazs
 */

#ifndef STEPPER_CALCULATION_H_
#define STEPPER_CALCULATION_H_

#include "dma_init.h"

struct decoded calc(struct message* dataframe);
void set_direction(struct decoded* dataframe);
void set_global_variables(struct decoded* dataframe);

struct decoded{
	unsigned long long TIME_QUANTUM_CYCLE;

	unsigned long long TRUNC_X;
	unsigned long long TRUNC_Y;
	unsigned long long TRUNC_Z;
	unsigned long long TRUNC_E;

	unsigned long long STEPS_X;
	unsigned long long STEPS_Y;
	unsigned long long STEPS_Z;
	unsigned long long STEPS_E;

	unsigned char status;         //1: fresh data / 0: stepped

	unsigned char DIR_X;
	unsigned char DIR_Y;
	unsigned char DIR_Z;
	unsigned char DIR_E;
};

extern struct decoded data[10];


extern unsigned long long truncx;
extern unsigned long long truncy;
extern unsigned long long truncz;
extern unsigned long long trunce;

extern unsigned long long time_quantum_cycle;

extern unsigned long long stepsx;
extern unsigned long long stepsy;
extern unsigned long long stepsz;
extern unsigned long long stepse;
//extern unsigned long long max;


#endif /* STEPPER_CALCULATION_H_ */
