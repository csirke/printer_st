/*
 * timer_init.h
 *
 *  Created on: 2018. dec. 12.
 *      Author: Balazs
 */

#ifndef TIMER_INIT_H_
#define TIMER_INIT_H_
#define P_CONST 5UL
#define I_CONST 1UL
#define STP_MM 500UL 		//stp/mm
#define MM_SEC 3UL			//mm/sec
#define STP_SEC STP_MM*MM_SEC //1500Hz
#define TIME_QANTUM STP_SEC * 32UL //a step divided into 32 timeframe ->48kHz interrupt firing

extern unsigned long desired_temp;
extern unsigned long long buff_truncx;
extern unsigned long long buff_truncy;
extern unsigned long long buff_truncz;
extern unsigned long long buff_trunce;
extern unsigned long long buff_time_quantum_cycle;

extern unsigned long long stepped_steps_x;
extern unsigned long long stepped_steps_y;
extern unsigned long long stepped_steps_z;
extern unsigned long long stepped_steps_e;

extern unsigned char stepping_in_progress; // 0 if stepping is not in progress, 1 if stepping is in progress

extern unsigned char index_to_read;
extern unsigned char index_to_step;

void init_timer1(void);
void init_timer2(void);
void init_timer3(void);

#endif /* TIMER_INIT_H_ */
