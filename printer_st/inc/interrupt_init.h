/*
 * interrupt_init.h
 *
 *  Created on: 2018. dec. 12.
 *      Author: Balazs
 */

#ifndef INTERRUPT_INIT_H_
#define INTERRUPT_INIT_H_

void interrupt_init_func(void);

#endif /* INTERRUPT_INIT_H_ */
